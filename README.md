N-Queens Search Algorithm Demonstration

Illustrates backtracking and local search on the problems of placing N queens on an N x N chessboard so that no two queens attack each other.

Written in Typescript.

Copyright 2020 by Henry Kautz <henry.kautz@gmail.com>.  This program may be freely used and modified for educational use.
