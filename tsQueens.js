/*
tsQueens
N-Queens demo programmed in Typescript.
Copyright 2020 Henry Kautz <henry.kautz@gmail.com>
This program may be freely redistributed and modified for educational uses.
*/
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
console.log("reading tsQueens.js");
var debug = false;
var usenode = false;
var Square;
(function (Square) {
    Square["Blank"] = "_";
    Square["Queen"] = "q";
})(Square || (Square = {}));
;
function bug(s) {
    if (debug) {
        console.log(s);
    }
}
function randomInteger(min, max) {
    if (max == min) {
        return min;
    }
    if (max < min) {
        throw 'Bad call to randomInteger(' + min + ',' + max + ')';
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function shuffle(array) {
    var _a;
    var currentIndex = array.length, randomIndex;
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
        // Pick a remaining element.
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        // And swap it with the current element.
        _a = [
            array[randomIndex], array[currentIndex]
        ], array[currentIndex] = _a[0], array[randomIndex] = _a[1];
    }
    return array;
}
function pad(s, size) {
    while (s.length < size) {
        s = " " + s;
    }
    return s;
}
function ntoi(num, digits) {
    if (digits === void 0) { digits = 6; }
    var s = String(num);
    return pad(s, digits);
}
var nQueens = /** @class */ (function () {
    function nQueens(n) {
        this.Dim = n;
        this.Board = new Array(this.Dim);
        this.QueenCol = new Array(this.Dim);
        this.FixedRow = new Array(this.Dim);
        this.Attacks = new Array(this.Dim);
        this.CreateBoard();
        this.ClearBoard();
        this.numberFixed = 0;
    }
    nQueens.prototype.CreateBoard = function () {
        for (var i = 0; i < this.Dim; i++) {
            this.Board[i] = new Array(this.Dim);
            this.Attacks[i] = new Array(this.Dim);
        }
    };
    nQueens.prototype.ClearBoard = function () {
        for (var i = 0; i < this.Dim; i++) {
            this.QueenCol[i] = -1;
            this.FixedRow[i] = false;
            for (var j = 0; j < this.Dim; j++) {
                this.Board[i][j] = Square.Blank;
                updateChessboard(i, j, Square.Blank);
                this.Attacks[i][j] = 0;
            }
        }
        this.numberFixed = 0;
        updateFixedInput(0);
    };
    nQueens.prototype.ClearUnfixedQueens = function () {
        for (var i = 0; i < this.Dim; i++) {
            if (!this.FixedRow[i]) {
                this.QueenCol[i] = -1;
            }
            for (var j = 0; j < this.Dim; j++) {
                if (!this.FixedRow[i] || j != this.QueenCol[i]) {
                    this.Board[i][j] = Square.Blank;
                    updateChessboard(i, j, Square.Blank);
                }
                this.Attacks[i][j] = 0;
            }
        }
    };
    nQueens.prototype.PrintBoard = function () {
        var s = [];
        for (var i = 0; i < this.Dim; i++) {
            for (var j = 0; j < this.Dim; j++) {
                s.push(this.Board[i][j], ' ');
            }
            s.push('\n');
        }
        s.push('\n');
        console.log('printing board\n');
        console.log(s.join(''));
    };
    nQueens.prototype.FixQueen = function (r, c, fixedcolor) {
        if (fixedcolor === void 0) { fixedcolor = false; }
        if (this.AttacksAt(r, c, true) > 0) {
            return false;
        }
        this.Board[r][c] = Square.Queen;
        updateChessboard(r, c, Square.Queen, fixedcolor);
        this.Board[r][c] = Square.Queen;
        this.QueenCol[r] = c;
        this.FixedRow[r] = true;
        this.numberFixed += 1;
        updateFixedInput(this.numberFixed);
        return true;
    };
    nQueens.prototype.unfixQueen = function (r, c) {
        if (this.Board[r][c] == Square.Queen) {
            this.Board[r][c] = Square.Blank;
            updateChessboard(r, c, Square.Blank);
            this.QueenCol[r] = -1;
            this.FixedRow[r] = false;
            this.numberFixed -= 1;
            updateFixedInput(this.numberFixed);
        }
    };
    nQueens.prototype.placeFixed = function (goalFixed) {
        if (this.numberFixed == goalFixed) {
            TellUser('Warning, nothing to place.');
            return;
        }
        if (this.numberFixed > goalFixed) {
            TellUser('Error, you must clear current fixed queens before placing fewer.');
            return;
        }
        if (goalFixed > this.Dim) {
            TellUser('Error, number to fix is greater than N.');
            return;
        }
        var toPlace = goalFixed - this.numberFixed;
        // Randomly place queens
        this.ClearUnfixedQueens();
        this.RandomlyPlaceSomeQueens(toPlace);
        HaltFlag = false;
        // Run local search
        if (!this.PartialLocalSearchSolver()) {
            TellUser('Sorry, could not find way to place fixed queens.');
            this.ClearUnfixedQueens(); // Add back in later ****************
            this.DisplayEntireBoard();
            return;
        }
        // Fix placed queens
        this.numberFixed = goalFixed;
        var nf = 0;
        for (var i = 0; i < this.Dim; i++) {
            if (this.QueenCol[i] > -1) { // don't look at empty rows
                this.FixedRow[i] = true;
                nf += 1;
            }
        }
        if (nf != goalFixed) {
            TellUser('CODE ERROR, fixed queen count mismatch');
            bug('Fixed queen count mismatch');
            debug && this.PrintBoard();
        }
        this.numberFixed = nf;
        // Display entire board
        this.DisplayEntireBoard();
        TellUser('Succeeded in fixing ' + String(this.numberFixed) + ' queens.');
    };
    nQueens.prototype.DisplayEntireBoard = function () {
        for (var r = 0; r < this.Dim; r++) {
            for (var c = 0; c < this.Dim; c++) {
                if (this.QueenCol[r] == -1 || c != this.QueenCol[r]) {
                    updateChessboard(r, c, Square.Blank);
                }
                else {
                    updateChessboard(r, c, Square.Queen, this.FixedRow[r]);
                }
            }
        }
    };
    nQueens.prototype.PrintAttacks = function () {
        console.log('Printing attacks\n');
        for (var r = 0; r < this.Dim; r++) {
            var s = ntoi(r + 1, 3) + ':  ';
            for (var c = 0; c < this.Dim; c++) {
                s = s + ' ' + ntoi(this.Attacks[r][c]);
            }
            console.log(s + '\n');
        }
    };
    nQueens.prototype.PartialLocalSearchSolver = function () {
        debug && console.log('QueenCol = ', this.QueenCol);
        for (var i = 1; i <= 100; i++) {
            if (HaltFlag) { // allow halt button to interrupt
                return false;
            }
            bug('Partial Local Search step ' + String(i));
            debug && this.PrintBoard();
            // Make list of rows with attacks > 0 that are not fixed
            var R = [];
            var totalAttacks = 0; // total attacks will be twice the number of attacking pairs
            for (var i_1 = 0; i_1 < this.Dim; i_1++) {
                if (this.QueenCol[i_1] > -1) { // don't look at empty rows
                    var thisRowAttacks = this.Attacks[i_1][this.QueenCol[i_1]];
                    if (thisRowAttacks < 0) {
                        TellUser('CODE ERROR: Attacks[' + String(i_1) + '][' + String(this.QueenCol[i_1]) +
                            '] is negative ' + String(thisRowAttacks));
                        return false;
                    }
                    totalAttacks += thisRowAttacks;
                    if (!this.FixedRow[i_1] && this.Attacks[i_1][this.QueenCol[i_1]] > 0) {
                        R.push(i_1);
                    }
                }
            }
            // Code check
            if (totalAttacks % 2 == 1) {
                TellUser('CODE ERROR, total attacks is odd = ' + String(totalAttacks));
                this.PrintBoard();
                this.PrintAttacks();
                return false;
            }
            // If no such rows, then nothing more to do
            if (R.length == 0) {
                HaltFlag = true;
                if (totalAttacks == 0) {
                    return true;
                }
                TellUser('CODE ERROR: empty R but totalAttacks = ' + String(totalAttacks));
                this.PrintAttacks();
                return false;
            }
            // Randomly pick a row with attacks
            var row = R[randomInteger(0, R.length - 1)];
            var col = this.QueenCol[row];
            var currentAttack = this.Attacks[row][col];
            // Flip a coin with probability 0.5, if heads move the queen to an empty row
            if (Math.random() < 0.5) {
                var Empty = [];
                // Make a list of empty rows
                for (var i_2 = 0; i_2 < this.Dim; i_2++) {
                    if (this.QueenCol[i_2] == -1) {
                        Empty.push(i_2);
                    }
                }
                debug && console.log('list of empty rows Empty is ', Empty);
                if (Empty.length > 0) {
                    // Pick an empty row
                    var newrow = Empty[randomInteger(0, Empty.length - 1)];
                    // Pick a random column in the new row
                    var newcol = randomInteger(0, this.Dim - 1);
                    // Remove queen from old spot
                    debug && console.log('before hop, source QueenCol[', row, '] is ', this.QueenCol[row]);
                    debug && console.log('before hop, dest QueenCol[', newrow, '] is ', this.QueenCol[newrow]);
                    this.updateAttacks(row, col, Square.Blank, true);
                    // Put queen in new spot
                    this.updateAttacks(newrow, newcol, Square.Queen, true);
                    debug && console.log('after hop, source QueenCol[', row, '] is ', this.QueenCol[row]);
                    debug && console.log('after hop, dest QueenCol[', newrow, '] is ', this.QueenCol[newrow]);
                    bug('PLS: queen hopped on step ' + i + ' from row ' + row + ' col ' + col + ' to row ' + newrow + ' col ' + newcol);
                    debug && this.PrintBoard();
                    row = newrow;
                    col = newcol;
                    // Check if we were lucky and there are no attacks
                    if (this.Attacks[row][col] == 0) {
                        continue;
                    }
                }
            }
            // Find the minimum number of attacks in columns other than current
            var minAttack = this.Dim * this.Dim;
            for (var j = 0; j < this.Dim; j++) {
                if (j != col && this.Attacks[row][j] < minAttack) {
                    minAttack = this.Attacks[row][j];
                }
            }
            // Make a list of the minimum attack columns
            var C = [];
            for (var j = 0; j < this.Dim; j++) {
                if (j != col && this.Attacks[row][j] == minAttack) {
                    C.push(j);
                }
            }
            // Move the queen
            var newCol = C[randomInteger(0, C.length - 1)];
            // Delete queen from current position and update Attacks
            this.updateAttacks(row, col, Square.Blank, true);
            // Add queen to new position and update Attacks
            this.updateAttacks(row, newCol, Square.Queen, true);
        }
        return false;
    };
    nQueens.prototype.RandomlyPlaceSomeQueens = function (k) {
        // Make list of empty rows
        var R = [];
        for (var r = 0; r < this.Dim; r++) {
            if (this.QueenCol[r] == -1) {
                R.push(r);
            }
        }
        shuffle(R);
        for (var i = 0; i < k; i++) {
            var row = R[i];
            var col = randomInteger(0, this.Dim - 1);
            this.QueenCol[row] = col;
            this.Board[row][col] = Square.Queen;
        }
        this.InitAttacks();
    };
    nQueens.prototype.RandomlyPlaceQueens = function () {
        for (var i = 0; i < this.Dim; i++) {
            if (this.QueenCol[i] < 0) {
                this.QueenCol[i] = randomInteger(0, this.Dim - 1);
                this.Board[i][this.QueenCol[i]] = Square.Queen;
            }
        }
        this.InitAttacks();
        for (var i = 0; i < this.Dim; i++) {
            updateChessboard(i, this.QueenCol[i], Square.Queen, this.FixedRow[i], true);
        }
    };
    nQueens.prototype.PlaceOrDeleteQueen = function (r, c) {
        if (this.Board[r][c] == Square.Queen) {
            this.unfixQueen(r, c);
        }
        else {
            if (!this.FixQueen(r, c, true)) {
                TellUser('Cannot place queen on attacked square!');
            }
        }
    };
    nQueens.prototype.AttacksAt = function (r, c, checkRowAttacks) {
        if (checkRowAttacks === void 0) { checkRowAttacks = false; }
        var a = 0;
        // S attacks row++
        for (var i = r + 1; 0 <= i && i < this.Dim; i++) {
            if (this.Board[i][c] == Square.Queen) {
                a++;
            }
        }
        // N attacks row--
        for (var i = r - 1; 0 <= i && i < this.Dim; i--) {
            if (this.Board[i][c] == Square.Queen) {
                a++;
            }
        }
        if (checkRowAttacks) {
            // E attacks col++
            for (var i = c + 1; 0 <= i && i < this.Dim; i++) {
                if (this.Board[r][i] == Square.Queen) {
                    a++;
                }
            }
            // W attacks row--
            for (var i = c - 1; 0 <= i && i < this.Dim; i--) {
                if (this.Board[r][i] == Square.Queen) {
                    a++;
                }
            }
        }
        // SE attacks row++ col++
        for (var i = r + 1, j = c + 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i++, j++) {
            if (this.Board[i][j] == Square.Queen) {
                a++;
            }
        }
        // NE attacks row-- col++
        for (var i = r - 1, j = c + 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i--, j++) {
            if (this.Board[i][j] == Square.Queen) {
                a++;
            }
        }
        // SW attacks row++ col--
        for (var i = r + 1, j = c - 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i++, j--) {
            if (this.Board[i][j] == Square.Queen) {
                a++;
            }
        }
        // NW attacks row-- col--
        for (var i = r - 1, j = c - 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i--, j--) {
            if (this.Board[i][j] == Square.Queen) {
                a++;
            }
        }
        // debug && console.log('checking attacks at ', r, ' ', c, ' attacks = ', a);
        return a;
    };
    nQueens.prototype.reColorQueens = function (r, c, quiet) {
        if (this.Board[r][c] == Square.Queen && !quiet) {
            updateChessboard(r, c, this.Board[r][c], this.FixedRow[r], true);
        }
    };
    nQueens.prototype.updateAttacks = function (r, c, sq, quiet) {
        if (quiet === void 0) { quiet = false; }
        /**
         * If removing queen, the squares that could be attacked all have their Attacks decremented.
         * If adding queen, the squares that could be attacked have all their Attacks incremented.
         * The attacks on (r,c) are unchanged.
         */
        var d = 0;
        this.Board[r][c] = sq;
        if (!quiet) {
            updateChessboard(r, c, sq, false, true);
        }
        if (sq == Square.Queen) {
            d = 1;
            this.QueenCol[r] = c;
        }
        else {
            d = -1;
            this.QueenCol[r] = -1;
        }
        // S attacks row++
        for (var i = r + 1, j = c; 0 <= i && i < this.Dim; i++) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i, j, quiet);
        }
        // N attacks row--
        for (var i = r - 1, j = c; 0 <= i && i < this.Dim; i--) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i, j, quiet);
        }
        // SE attacks row++ col++
        for (var i = r + 1, j = c + 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i++, j++) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i, j, quiet);
        }
        // NE attacks row-- col++
        for (var i = r - 1, j = c + 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i--, j++) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i, j, quiet);
        }
        // SW attacks row++ col--
        for (var i = r + 1, j = c - 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i++, j--) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i, j, quiet);
        }
        // NW attacks row-- col--
        for (var i = r - 1, j = c - 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i--, j--) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i, j, quiet);
        }
    };
    nQueens.prototype.InitAttacks = function () {
        for (var i = 0; i < this.Dim; i++) {
            for (var j = 0; j < this.Dim; j++) {
                this.Attacks[i][j] = this.AttacksAt(i, j);
            }
        }
    };
    nQueens.prototype.StartBacktrackingSolver = function (rand, restarts) {
        if (rand === void 0) { rand = false; }
        if (restarts === void 0) { restarts = false; }
        HaltFlag = false;
        RestartFlag = false;
        this.randomBacktracking = rand;
        this.RandomRestarts = restarts;
        this.cutoff = getCutoff();
        this.step = 0;
        this.ClearUnfixedQueens();
        debug && console.log('Creating generator with this.randomBacktracking = ', this.randomBacktracking, ' this.RandomRestarts = ', this.RandomRestarts);
        BacktrackGenerator = this.BacktrackSolver(0);
        bug('scheduling first call to BacktrackingSolverStepper');
        Schedule(BacktrackingSolverStepper);
        if (this.RandomRestarts) {
            TellUser('Restart ' + restartNumber);
        }
        else {
            TellUser('Starting ' + (this.randomBacktracking ? 'randomized ' : '') + ' backtracking search.');
        }
    };
    nQueens.prototype.BacktrackSolver = function (rowToPlace) {
        var status, row, i, k, col;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    debug && this.PrintBoard();
                    if (!(rowToPlace >= this.Dim)) return [3 /*break*/, 2];
                    TellUser((this.randomBacktracking ? 'Randomized backtracking ' : 'Backtracking ') +
                        'search succeeded using ' + this.step + ' backtracks.');
                    HaltFlag = true;
                    return [4 /*yield*/, true];
                case 1:
                    _a.sent();
                    // TellUser("CODE ERROR: unreachable block reached.")
                    return [2 /*return*/, true]; // Should never reach here
                case 2:
                    row = rowToPlace;
                    return [4 /*yield*/, true];
                case 3:
                    _a.sent();
                    // Try each column in this row, return true if any succeed
                    this.columnList = new Array(this.Dim);
                    for (i = 0; i < this.Dim; i++) {
                        this.columnList[i] = i;
                    }
                    if (this.randomBacktracking) {
                        shuffle(this.columnList);
                    }
                    k = 0;
                    _a.label = 4;
                case 4:
                    if (!(k < this.Dim)) return [3 /*break*/, 8];
                    col = this.columnList[k];
                    if (!(this.AttacksAt(row, col) == 0 &&
                        (!this.FixedRow[row] || col == this.QueenCol[row]))) return [3 /*break*/, 7];
                    if (!this.FixedRow[row]) {
                        this.Board[row][col] = Square.Queen;
                        updateChessboard(row, col, Square.Queen);
                    }
                    return [5 /*yield**/, __values(this.BacktrackSolver(rowToPlace + 1))];
                case 5:
                    status = _a.sent();
                    debug && console.log('recursive call to BacktrackSolver returns ', status);
                    if (status) {
                        debug && console.log('immediate return on success');
                        return [2 /*return*/, true];
                    }
                    debug && console.log('continuing search on failure');
                    if (!this.FixedRow[row]) {
                        this.Board[row][col] = Square.Blank;
                        updateChessboard(row, col, Square.Blank);
                    }
                    this.step += 1;
                    debug && console.log('backtracks = ', this.step);
                    updateStep(this.step);
                    if (!(this.step >= this.cutoff)) return [3 /*break*/, 7];
                    if (this.RandomRestarts) {
                        TellUser('    Restart fails.');
                        RestartFlag = true;
                    }
                    else {
                        TellUser((this.randomBacktracking ? 'Randomized backtracking ' : 'Backtracking ') +
                            'search failed after ' + nqueens.step + ' backtracks.');
                        HaltFlag = true;
                    }
                    return [4 /*yield*/, false];
                case 6:
                    _a.sent();
                    // TellUser("CODE ERROR: unreachable block reached.")
                    return [2 /*return*/, false]; // Should not reach here
                case 7:
                    k++;
                    return [3 /*break*/, 4];
                case 8:
                    debug && console.log('Cannot place queens in row ', rowToPlace);
                    // All columns failed so return 
                    if (rowToPlace == 0) {
                        HaltFlag = true;
                        TellUser('Backtracking search proves no solution exists.');
                    }
                    return [4 /*yield*/, true];
                case 9:
                    _a.sent();
                    return [2 /*return*/, false];
            }
        });
    };
    nQueens.prototype.StartLocalSearchSolver = function () {
        this.ClearUnfixedQueens();
        this.cutoff = getCutoff();
        this.RandomlyPlaceQueens();
        this.step = 0;
        updateStep(this.step);
        HaltFlag = false;
        Schedule(LocalSearchSolverStepper);
        TellUser('Starting local search.');
    };
    nQueens.prototype.LocalSearchSolver = function () {
        debug && console.log('Entering local search solver for step ', this.step);
        if (this.step > this.cutoff) {
            // Failed to find solution
            TellUser('Local Search failed after ' + this.step + ' steps.');
            HaltFlag = true;
            return;
        }
        debug && this.PrintBoard();
        // Make list of rows with attacks > 0 that are not fixed
        var R = [];
        var totalAttacks = 0; // total attacks will be twice the number of attacking pairs
        for (var i = 0; i < this.Dim; i++) {
            totalAttacks += this.Attacks[i][this.QueenCol[i]];
            if (!this.FixedRow[i] && this.Attacks[i][this.QueenCol[i]] > 0) {
                R.push(i);
            }
        }
        // If no such rows, then nothing more to do
        if (R.length == 0) {
            HaltFlag = true;
            if (totalAttacks == 0) {
                TellUser('Local search succeeded using ' + this.step + ' steps.');
                return;
            }
            TellUser('Error, fixed queens are attacking each other!');
            return;
        }
        // Randomly pick a row with attacks
        var row = R[randomInteger(0, R.length - 1)];
        var col = this.QueenCol[row];
        var currentAttack = this.Attacks[row][col];
        // Find the minimum number of attacks in columns other than current
        var minAttack = this.Dim * this.Dim;
        for (var j = 0; j < this.Dim; j++) {
            if (j != col && this.Attacks[row][j] < minAttack) {
                minAttack = this.Attacks[row][j];
            }
        }
        // Make a list of the minimum attack columns
        var C = [];
        for (var j = 0; j < this.Dim; j++) {
            if (j != col && this.Attacks[row][j] == minAttack) {
                C.push(j);
            }
        }
        debug && console.log('flip = ', this.step, ' totalAttacks = ', totalAttacks, ' currentAttack = ', currentAttack, ' minAttack = ', minAttack, ' delta = ', minAttack - currentAttack);
        // Move the queen
        var newCol = C[randomInteger(0, C.length - 1)];
        // Delete queen from current position and update Attacks
        this.updateAttacks(row, col, Square.Blank);
        // Add queen to new position and update Attacks
        this.updateAttacks(row, newCol, Square.Queen);
        debug && console.log('    Move (', row, ',', col, ') to (', row, ',', newCol, ')');
        this.step++;
        updateStep(this.step);
        // get ready for next step
        if (HaltFlag) {
            TellUser('Local search halted at step ' + this.step + '.');
            return;
        }
        Schedule(LocalSearchSolverStepper);
    };
    return nQueens;
}());
// Global Variables
var nqueens;
var delay = 500; // milliseconds between solver steps
var colorFlag = false;
var HaltFlag = true;
var RestartFlag = false;
var restartNumber = 0;
var cutoff = 200; // Only used in usenode mode
var BacktrackGenerator; // Generator object for backtracking search
// Interface Functions
function Schedule(fn) {
    if (usenode) {
        setTimeout(fn, delay);
    }
    else {
        window.setTimeout(fn, delay);
    }
}
function BacktrackingSolverStepper() {
    bug('About to call BacktrackGenerator.next');
    var status = BacktrackGenerator.next();
    debug && console.log('  next() returns ', status);
    if (!HaltFlag) {
        if (RestartFlag) {
            restartNumber += 1;
            updateRestart(restartNumber);
            Schedule(initRandRestarts);
            RestartFlag = false;
        }
        else {
            Schedule(BacktrackingSolverStepper);
        }
    }
}
function initRandRestarts() {
    nqueens.StartBacktrackingSolver(true, true);
}
function LocalSearchSolverStepper() {
    nqueens.LocalSearchSolver();
}
function TellUser(msg) {
    if (usenode) {
        console.log(msg);
    }
    else {
        var b = document.getElementById('solverResult');
        b.innerHTML = b.innerHTML + '<br />\n' + msg;
        b.scrollTop = b.scrollHeight;
    }
}
function attackColor(a) {
    // convert a number 0 to max number of attacks to a color string
    if (a <= 0) {
        return 'GREEN';
    }
    //   else if (a <= 1) { return '#00D3DB'; }
    else if (a <= 1) {
        return 'BLUE';
    }
    else if (a <= 2) {
        return 'PURPLE';
    }
    else {
        return 'RED';
    }
}
function updateStep(n) {
    var counter = document.getElementById("step");
    counter.innerHTML = n.toString();
}
function updateRestart(n) {
    var counter = document.getElementById("restart");
    counter.innerHTML = n.toString();
}
function updateChessboard(r, c, sq, isFixed, useColors) {
    if (isFixed === void 0) { isFixed = false; }
    if (useColors === void 0) { useColors = false; }
    if (usenode) {
        debug && console.log('Chessboard[', r, '][', c, '] = ', sq);
    }
    else {
        var id = 'sq-' + r.toString() + '-' + c.toString();
        // debug && console.log('setting cell=', id, " to ", sq);
        var cell = document.getElementById(id);
        if (sq == Square.Blank) {
            cell.innerHTML = '';
        }
        else {
            // Insert the character for Queen
            var queenchar = isFixed ? '&#9813;' : '&#9819;';
            if (colorFlag && useColors) {
                cell.innerHTML = '<div style="color:' + attackColor(nqueens.Attacks[r][c]) +
                    '">' + queenchar + '</div>';
            }
            else {
                cell.innerHTML = queenchar;
            }
        }
    }
}
function generateChessboardHTML(Dim) {
    var h = [];
    for (var r = 0; r < Dim; r++) {
        h.push('<tr>\n');
        for (var c = 0; c < Dim; c++) {
            h.push('<td id="sq-', r.toString(), '-', c.toString(), '" name="chessBoardSquare" class="');
            // calculate square color
            // 0,0 light 0,1 dark 0,2 light ...
            // 1,0 dark 1,1 light 1,2 dark ...
            // r+c even is light, r+c odd is dark
            if ((r + c) % 2 == 0) {
                h.push('light');
            }
            else {
                h.push('dark');
            }
            h.push('"></td>\n');
        }
        h.push('</tr>\n');
    }
    return h.join('');
}
function updateSquareStyleDim(d) {
    var h = document.documentElement.clientHeight; // units are px
    var sqSize = Math.floor(Math.max(((h - 10) / d) - 4, 8)); // Allow 2 px for border
    document.getElementById('squareSizeStyle').innerHTML =
        '.chess-board td { width:' + sqSize + 'px; height:' + sqSize + 'px; font-size:' +
            (sqSize - 2) + 'px; text-align: center; line-height: ' + (sqSize - 2) + 'px; }';
    debug && console.log('creating style ', document.getElementById('squareSizeStyle').innerHTML);
}
function updateSquareStyle() {
    updateSquareStyleDim(nqueens.Dim);
}
function resizeBoard(d) {
    // Can't set dim before creating board, but creating the board 
    updateSquareStyleDim(d);
    document.getElementById('chess-board-body').innerHTML = generateChessboardHTML(d);
    // Add handler to each square
    var elements = document.getElementsByName('chessBoardSquare');
    // elements is a nodeList, not an array, so can't use for ... of
    for (var i = 0; i < elements.length; i++) {
        elements[i].onclick = function () { squareHandler(this.id); };
    }
    // debug && console.log('creating board ', document.getElementById('chess-board-body').innerHTML);
    // Can't create nQueens until the HTML has been created for it
    nqueens = new nQueens(d);
    updateStep(0);
}
// Button Handlers
function updateFixedInput(numFixed) {
    document.getElementById('inputFixed').value = String(numFixed);
}
function getCutoff() {
    if (usenode) {
        return cutoff;
    }
    return Number(document.getElementById('inputCutoff').value);
}
function resizeButtonHandler() {
    // Handler for resize button
    resizeBoard(Number(document.getElementById('inputDim').value));
}
function squareHandler(id) {
    var _a;
    var sq, rs, cs;
    debug && console.log('got click on ', id);
    _a = id.split('-'), sq = _a[0], rs = _a[1], cs = _a[2];
    debug && console.log('calling placeOrDeleteQueen at ', Number(rs), ' ', Number(cs));
    nqueens.PlaceOrDeleteQueen(Number(rs), Number(cs));
}
function haltButtonHandler() {
    HaltFlag = true;
    debug && console.log('clicked on Halt');
    TellUser('Search halted by user.');
}
function colorButtonHandler(isChecked) {
    colorFlag = isChecked;
    debug && console.log('Color checkbox set to ', isChecked);
    if (isChecked) {
        TellUser('Attacks color code:');
        TellUser('  0 = Green');
        TellUser('  1 = Blue');
        TellUser('  2 = Purple');
        TellUser('  3 or more = Red');
    }
}
function localSearchButtonHandler() {
    nqueens.StartLocalSearchSolver();
}
function clearButtonHandler() {
    nqueens.ClearBoard();
    updateStep(0);
}
function backtrackingButtonHandler() {
    nqueens.StartBacktrackingSolver(false);
}
function randomBacktrackingButtonHandler() {
    nqueens.StartBacktrackingSolver(true);
}
function randomRestartsButtonHandler() {
    restartNumber = 0;
    updateRestart(restartNumber);
    nqueens.StartBacktrackingSolver(true, true);
}
function placeButtonHandler() {
    var goalFixed = Number(document.getElementById('inputFixed').value);
    nqueens.placeFixed(goalFixed);
}
function delayButtonHandler(value) {
    delay = Number(value) * 175;
    debug && console.log('delay set to ', delay);
}
if (usenode) {
    delay = 0;
    cutoff = 200;
    nqueens = new nQueens(6);
    nqueens.FixQueen(0, 4);
    nqueens.StartBacktrackingSolver(false);
}
function finishBuildingHandlers() {
    console.log("started window.onload");
    document.getElementById('backtrackingButton').onclick = function () { backtrackingButtonHandler(); };
    document.getElementById('randBacktrackingButton').onclick = function () { randomBacktrackingButtonHandler(); };
    document.getElementById('randomRestartsButton').onclick = function () { randomRestartsButtonHandler(); };
    document.getElementById('haltButton').onclick = function () { haltButtonHandler(); };
    document.getElementById('placeButton').onclick = function () { placeButtonHandler(); };
    document.getElementById('resizeButton').onclick = function () { resizeButtonHandler(); };
    document.getElementById('clearButton').onclick = function () { clearButtonHandler(); };
    document.getElementById('localSearchButton').onclick = function () { localSearchButtonHandler(); };
    document.getElementById('colorAttacks').onchange = function () { colorButtonHandler(this.checked); };
    var elements = document.getElementsByName('delay');
    // elements is a nodeList, not an array, so can't use for ... of
    for (var i = 0; i < elements.length; i++) {
        elements[i].onchange = function () { delayButtonHandler(this.value); };
    }
    resizeBoard(8);
    window.addEventListener("resize", updateSquareStyle);
}
window.addEventListener('load', function () { finishBuildingHandlers(); });
// The 'load' event does not fire in StackBlitz, so initialization must be called directly
// by uncommenting the following line:
// finishBuildingHandlers();
