/*
tsQueens
N-Queens demo programmed in Typescript.
Copyright 2020 Henry Kautz <henry.kautz@gmail.com>
This program may be freely redistributed and modified for educational uses.
*/

console.log("reading tsQueens.js");

const debug: boolean = false;
const usenode: boolean = false;
enum Square { Blank = '_', Queen = 'q' };

function bug(s:string){
    if (debug){
        console.log(s);
    }
}

function randomInteger(min:number, max:number) {
    if (max == min) { return min; }
    if (max < min) {
        throw 'Bad call to randomInteger(' + min + ',' + max + ')';
    }
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function shuffle(array:any[]) {
    let currentIndex = array.length,  randomIndex:number;
    // While there remain elements to shuffle.
    while (currentIndex != 0) {
      // Pick a remaining element.
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;
      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }
    return array;
}

function pad(s:string, size:number): string {
    while (s.length < size) {
        s = " " + s;
    }
    return s;
}

function ntoi(num:number, digits:number = 6): string {
    let s = String(num);
    return pad(s,digits);
}

class nQueens {

    Dim: number;
    Board: Square[][];
    private QueenCol: number[];
    private FixedRow: boolean[];
    Attacks: number[][];
    private cutoff: number;
    step: number;
    private randomBacktracking: boolean;
    private columnList: number[];
    private numberFixed: number;
    RandomRestarts: boolean;

    constructor(n: number) {
        this.Dim = n;
        this.Board = new Array(this.Dim);
        this.QueenCol = new Array(this.Dim);
        this.FixedRow = new Array(this.Dim);
        this.Attacks = new Array(this.Dim);
        this.CreateBoard();
        this.ClearBoard();
        this.numberFixed = 0;

    }

    private CreateBoard() {
        for (let i = 0; i < this.Dim; i++) {
            this.Board[i] = new Array(this.Dim);
            this.Attacks[i] = new Array(this.Dim);
        }
    }

    ClearBoard() {
        for (let i = 0; i < this.Dim; i++) {
            this.QueenCol[i] = -1;
            this.FixedRow[i] = false;
            for (let j = 0; j < this.Dim; j++) {
                this.Board[i][j] = Square.Blank;
                updateChessboard(i, j, Square.Blank);
                this.Attacks[i][j] = 0;
            }
        }
        this.numberFixed = 0;
        updateFixedInput(0);
    }

    ClearUnfixedQueens() {
        for (let i = 0; i < this.Dim; i++) {
            if (!this.FixedRow[i]) {
                this.QueenCol[i] = -1;
            }
            for (let j = 0; j < this.Dim; j++) {
                if (!this.FixedRow[i] || j != this.QueenCol[i]) {
                    this.Board[i][j] = Square.Blank;
                    updateChessboard(i, j, Square.Blank);
                }
                this.Attacks[i][j] = 0;
            }
        }
    }

    PrintBoard() {
        let s: string[] = [];
        for (let i = 0; i < this.Dim; i++) {
            for (let j = 0; j < this.Dim; j++) {
                s.push(this.Board[i][j], ' ');
            }
            s.push('\n');
        }
        s.push('\n');
        console.log('printing board\n');
        console.log(s.join(''));
    }

    FixQueen(r: number, c: number, fixedcolor: boolean = false): boolean {
        if (this.AttacksAt(r, c, true) > 0) {
            return false;
        }
        this.Board[r][c] = Square.Queen;
        updateChessboard(r, c, Square.Queen, fixedcolor);
        this.Board[r][c] = Square.Queen;
        this.QueenCol[r] = c;
        this.FixedRow[r] = true;
        this.numberFixed += 1;
        updateFixedInput(this.numberFixed);
        return true;
    }

    private unfixQueen(r: number, c: number) {
        if (this.Board[r][c] == Square.Queen) {
            this.Board[r][c] = Square.Blank;
            updateChessboard(r, c, Square.Blank);
            this.QueenCol[r] = -1;
            this.FixedRow[r] = false;
            this.numberFixed -= 1;
            updateFixedInput(this.numberFixed);
        }
    }

    placeFixed(goalFixed:number){
        if (this.numberFixed == goalFixed){
            TellUser('Warning, nothing to place.')
            return;
        }
        if (this.numberFixed > goalFixed){
            TellUser('Error, you must clear current fixed queens before placing fewer.')
            return;
        }
        if (goalFixed > this.Dim){
            TellUser('Error, number to fix is greater than N.')
            return;
        }
        let toPlace = goalFixed - this.numberFixed;
        // Randomly place queens
        this.ClearUnfixedQueens();
        this.RandomlyPlaceSomeQueens(toPlace);
        HaltFlag = false;
        // Run local search
        if (! this.PartialLocalSearchSolver()){
            TellUser('Sorry, could not find way to place fixed queens.')
            this.ClearUnfixedQueens();  // Add back in later ****************
            this.DisplayEntireBoard();
            return;
        }
        // Fix placed queens
        this.numberFixed = goalFixed;
        let nf = 0;
        for (let i = 0; i < this.Dim; i++) {
            if (this.QueenCol[i] > -1){ // don't look at empty rows
                this.FixedRow[i] = true;
                nf += 1;
            }
        }
        if (nf != goalFixed){
            TellUser('CODE ERROR, fixed queen count mismatch');
            bug('Fixed queen count mismatch');
            debug && this.PrintBoard();
        }
        this.numberFixed = nf;
        // Display entire board

        this.DisplayEntireBoard();

        TellUser('Succeeded in fixing ' + String(this.numberFixed) + ' queens.')
    }

    DisplayEntireBoard(){  
        for (let r = 0; r < this.Dim; r++) {
            for (let c = 0; c < this.Dim; c++){
                if (this.QueenCol[r]==-1 || c != this.QueenCol[r] ){
                    updateChessboard(r,c,Square.Blank);
                }
                else {
                    updateChessboard(r,c,Square.Queen, this.FixedRow[r])
                }
            }
        }
    }

    PrintAttacks(){
        console.log('Printing attacks\n');
        
        for (let r = 0; r<this.Dim; r++){
            let s = ntoi(r+1,3) + ':  ';
            for (let c = 0; c<this.Dim; c++){
                s = s + ' ' + ntoi(this.Attacks[r][c]);
            }
            console.log(s + '\n');
        }
    }

    private PartialLocalSearchSolver(): boolean {
    
        debug && console.log('QueenCol = ', this.QueenCol);

        for (let i = 1; i<=100; i++){
            if (HaltFlag) { // allow halt button to interrupt
                return false;
            }

            bug('Partial Local Search step ' + String(i));
            debug && this.PrintBoard();

            // Make list of rows with attacks > 0 that are not fixed
            let R: number[] = [];
            let totalAttacks: number = 0; // total attacks will be twice the number of attacking pairs
            for (let i = 0; i < this.Dim; i++) {
                if (this.QueenCol[i] > -1){ // don't look at empty rows

                    let thisRowAttacks = this.Attacks[i][this.QueenCol[i]];
                    if (thisRowAttacks<0){
                        TellUser('CODE ERROR: Attacks['+String(i)+']['+String(this.QueenCol[i]) +
                        '] is negative ' + String(thisRowAttacks));
                        return false;
                    }

                    totalAttacks += thisRowAttacks;
                    if (!this.FixedRow[i] && this.Attacks[i][this.QueenCol[i]] > 0) { 
                        R.push(i); }
                }
            }
            // Code check
            if (totalAttacks%2 == 1){
                TellUser('CODE ERROR, total attacks is odd = ' + String(totalAttacks));
                this.PrintBoard();
                this.PrintAttacks();
                return false;
            }
            // If no such rows, then nothing more to do
            if (R.length == 0) {
                HaltFlag = true;
                if (totalAttacks == 0) {
                    return true;
                }

                TellUser('CODE ERROR: empty R but totalAttacks = ' + String(totalAttacks));
                this.PrintAttacks();

                return false;
            }
            // Randomly pick a row with attacks
            let row: number = R[randomInteger(0, R.length - 1)];
            let col: number = this.QueenCol[row];
            let currentAttack: number = this.Attacks[row][col];

            // Flip a coin with probability 0.5, if heads move the queen to an empty row
            if (Math.random()<0.5){

                let Empty: number[] = [];
                // Make a list of empty rows
                for (let i = 0; i < this.Dim; i++) {
                    if (this.QueenCol[i] == -1){ 
                        Empty.push(i); 
                    }
                }

                debug && console.log('list of empty rows Empty is ', Empty);

                if (Empty.length > 0){
                    // Pick an empty row
                    let newrow = Empty[randomInteger(0, Empty.length - 1)];
                    // Pick a random column in the new row
                    let newcol = randomInteger(0, this.Dim - 1);
                    // Remove queen from old spot

                    debug && console.log('before hop, source QueenCol[', row, '] is ', this.QueenCol[row]);
                    debug && console.log('before hop, dest QueenCol[', newrow, '] is ', this.QueenCol[newrow]);

                    this.updateAttacks(row,col,Square.Blank,true);
                    // Put queen in new spot
                    this.updateAttacks(newrow,newcol,Square.Queen,true); 
                    
                    debug && console.log('after hop, source QueenCol[', row, '] is ', this.QueenCol[row]);
                    debug && console.log('after hop, dest QueenCol[', newrow, '] is ', this.QueenCol[newrow]);

                    bug('PLS: queen hopped on step ' + i + ' from row ' + row + ' col ' + col + ' to row ' + newrow + ' col ' + newcol);
                    debug && this.PrintBoard();

                    row = newrow;
                    col = newcol;
                    // Check if we were lucky and there are no attacks
                    if (this.Attacks[row][col] == 0){
                        continue;
                    }
                }   
            }

            // Find the minimum number of attacks in columns other than current
            let minAttack = this.Dim * this.Dim;
            for (let j = 0; j < this.Dim; j++) {
                if (j != col && this.Attacks[row][j] < minAttack) {
                    minAttack = this.Attacks[row][j];
                }
            }
            // Make a list of the minimum attack columns
            let C: number[] = [];
            for (let j = 0; j < this.Dim; j++) {
                if (j != col && this.Attacks[row][j] == minAttack) {
                    C.push(j);
                }
            }
            // Move the queen
            let newCol: number = C[randomInteger(0, C.length - 1)];
            // Delete queen from current position and update Attacks
            this.updateAttacks(row, col, Square.Blank, true);
            // Add queen to new position and update Attacks
            this.updateAttacks(row, newCol, Square.Queen, true);
        }
        return false;
    }

    private RandomlyPlaceSomeQueens(k:number) {

        // Make list of empty rows
        let R: number[] = [];
        for (let r = 0; r < this.Dim; r++) {
            if (this.QueenCol[r] == -1){ 
                R.push(r); 
            }
        }
        shuffle(R);
        for (let i = 0; i < k; i++) {
            let row = R[i];
            let col = randomInteger(0, this.Dim - 1);
            this.QueenCol[row] = col;
            this.Board[row][col] = Square.Queen;
        }
        this.InitAttacks();
    }
    
    
    private RandomlyPlaceQueens() {
        for (let i = 0; i < this.Dim; i++) {
            if (this.QueenCol[i] < 0) {
                this.QueenCol[i] = randomInteger(0, this.Dim - 1);
                this.Board[i][this.QueenCol[i]] = Square.Queen;
            }
        }
        this.InitAttacks();
        for (let i = 0; i < this.Dim; i++) {
            updateChessboard(i, this.QueenCol[i], Square.Queen,
                this.FixedRow[i], true);
        }
    }

    PlaceOrDeleteQueen(r: number, c: number) {
        if (this.Board[r][c] == Square.Queen) {
            this.unfixQueen(r, c);
        }
        else {
            if (!this.FixQueen(r, c, true)) {
                TellUser('Cannot place queen on attacked square!')
            }
        }
    }

    private AttacksAt(r: number, c: number, checkRowAttacks: boolean = false): number {
        let a = 0;
        // S attacks row++
        for (let i = r + 1; 0 <= i && i < this.Dim; i++) {
            if (this.Board[i][c] == Square.Queen) { a++; }
        }
        // N attacks row--
        for (let i = r - 1; 0 <= i && i < this.Dim; i--) {
            if (this.Board[i][c] == Square.Queen) { a++; }
        }
        if (checkRowAttacks) {
            // E attacks col++
            for (let i = c + 1; 0 <= i && i < this.Dim; i++) {
                if (this.Board[r][i] == Square.Queen) {
                    a++;
                }
            }
            // W attacks row--
            for (let i = c - 1; 0 <= i && i < this.Dim; i--) {
                if (this.Board[r][i] == Square.Queen) {
                    a++;
                }
            }
        }
        // SE attacks row++ col++
        for (let i = r + 1, j = c + 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i++, j++) {
            if (this.Board[i][j] == Square.Queen) { a++; }
        }
        // NE attacks row-- col++
        for (let i = r - 1, j = c + 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i--, j++) {
            if (this.Board[i][j] == Square.Queen) { a++; }
        }
        // SW attacks row++ col--
        for (let i = r + 1, j = c - 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i++, j--) {
            if (this.Board[i][j] == Square.Queen) { a++; }
        }
        // NW attacks row-- col--
        for (let i = r - 1, j = c - 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i--, j--) {
            if (this.Board[i][j] == Square.Queen) { a++; }
        }
        // debug && console.log('checking attacks at ', r, ' ', c, ' attacks = ', a);
        return a;
    }

    private reColorQueens(r:number, c:number, quiet:boolean){
        if (this.Board[r][c]==Square.Queen && !quiet){
            updateChessboard(r,c, this.Board[r][c], this.FixedRow[r], true);
        }
    }


    private updateAttacks(r: number, c: number, sq: Square, quiet=false) {
        /**
         * If removing queen, the squares that could be attacked all have their Attacks decremented. 
         * If adding queen, the squares that could be attacked have all their Attacks incremented.
         * The attacks on (r,c) are unchanged.
         */
        let d = 0;
        this.Board[r][c] = sq;
        if (! quiet){ updateChessboard(r, c, sq, false, true); }
        if (sq == Square.Queen) {
            d = 1;
            this.QueenCol[r] = c;
        }
        else {
            d = -1;
            this.QueenCol[r] = -1;
        }
        // S attacks row++
        for (let i = r + 1, j = c; 0 <= i && i < this.Dim; i++) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i,j,quiet);
        }
        // N attacks row--
        for (let i = r - 1, j = c; 0 <= i && i < this.Dim; i--) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i,j,quiet);
        }
        // SE attacks row++ col++
        for (let i = r + 1, j = c + 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i++, j++) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i,j,quiet);
        }
        // NE attacks row-- col++
        for (let i = r - 1, j = c + 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i--, j++) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i,j,quiet);
        }
        // SW attacks row++ col--
        for (let i = r + 1, j = c - 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i++, j--) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i,j,quiet);
        }
        // NW attacks row-- col--
        for (let i = r - 1, j = c - 1; 0 <= i && i < this.Dim && 0 <= j && j < this.Dim; i--, j--) {
            this.Attacks[i][j] += d;
            this.reColorQueens(i,j,quiet);
        }
    }

    private InitAttacks() {
        for (let i = 0; i < this.Dim; i++) {
            for (let j = 0; j < this.Dim; j++) {
                this.Attacks[i][j] = this.AttacksAt(i, j);
            }
        }
    }

    StartBacktrackingSolver(rand:boolean = false, restarts:boolean = false) {
        HaltFlag = false;
        RestartFlag = false;
        this.randomBacktracking = rand;
        this.RandomRestarts = restarts;
        this.cutoff = getCutoff();
        this.step = 0;
        this.ClearUnfixedQueens();
        debug && console.log('Creating generator with this.randomBacktracking = ', this.randomBacktracking, ' this.RandomRestarts = ',this.RandomRestarts);
        BacktrackGenerator = this.BacktrackSolver(0);
        bug('scheduling first call to BacktrackingSolverStepper');
        Schedule(BacktrackingSolverStepper);
        if (this.RandomRestarts){
            TellUser('Restart ' + restartNumber);
        }
        else {
            TellUser('Starting ' + (this.randomBacktracking ? 'randomized ' : '') + ' backtracking search.');
        }

    }

    * BacktrackSolver(rowToPlace: number) {
        let status: boolean;
        debug && this.PrintBoard();
        if (rowToPlace >= this.Dim) {
            TellUser((this.randomBacktracking ? 'Randomized backtracking ' : 'Backtracking ') + 
                'search succeeded using ' + this.step + ' backtracks.');
            HaltFlag = true;
            yield true;
            // TellUser("CODE ERROR: unreachable block reached.")
            return true; // Should never reach here
        }
        let row = rowToPlace; // index of the next queen to place

        yield true;

        // Try each column in this row, return true if any succeed
        
        this.columnList = new Array(this.Dim);
        for (let i=0;i<this.Dim;i++){
            this.columnList[i] = i;
        }
        if (this.randomBacktracking) {
            shuffle(this.columnList);
        }

        for (let k = 0; k < this.Dim; k++) {
            let col = this.columnList[k];
            if (this.AttacksAt(row, col) == 0 &&
                (!this.FixedRow[row] || col == this.QueenCol[row])) {
                if (!this.FixedRow[row]) {
                    this.Board[row][col] = Square.Queen;
                    updateChessboard(row, col, Square.Queen);
                }
                status = yield* this.BacktrackSolver(rowToPlace + 1);
                debug && console.log('recursive call to BacktrackSolver returns ', status);
                if (status) {
                    debug && console.log('immediate return on success');
                    return true;
                }
                debug && console.log('continuing search on failure');
                if (!this.FixedRow[row]) {
                    this.Board[row][col] = Square.Blank;
                    updateChessboard(row, col, Square.Blank);
                }
                this.step += 1;
                debug && console.log('backtracks = ', this.step)
                updateStep(this.step);

                if (this.step >= this.cutoff) {


                    
                    if (this.RandomRestarts){
                        TellUser('    Restart fails.');
                        RestartFlag = true;
                    }
                    else {
                        TellUser((this.randomBacktracking ? 'Randomized backtracking ' : 'Backtracking ') + 
                            'search failed after ' + nqueens.step + ' backtracks.')
                        HaltFlag = true;
                    }

                    yield false;

                    // TellUser("CODE ERROR: unreachable block reached.")
                    return false; // Should not reach here
                }
            }
        }
        debug && console.log('Cannot place queens in row ', rowToPlace);
        // All columns failed so return 
        if (rowToPlace == 0) {
            HaltFlag = true;
            TellUser('Backtracking search proves no solution exists.');
        }
        yield true
        return false;
    }

    StartLocalSearchSolver() {
        this.ClearUnfixedQueens();
        this.cutoff = getCutoff();
        this.RandomlyPlaceQueens();
        this.step = 0;
        updateStep(this.step);
        HaltFlag = false;
        Schedule(LocalSearchSolverStepper);
        TellUser('Starting local search.');
    }

    LocalSearchSolver() {
        debug && console.log('Entering local search solver for step ', this.step);
        if (this.step > this.cutoff) {
            // Failed to find solution
            TellUser('Local Search failed after ' + this.step + ' steps.');
            HaltFlag = true;
            return;
        }

        debug && this.PrintBoard();
        // Make list of rows with attacks > 0 that are not fixed
        let R: number[] = [];
        let totalAttacks: number = 0; // total attacks will be twice the number of attacking pairs
        for (let i = 0; i < this.Dim; i++) {
            totalAttacks += this.Attacks[i][this.QueenCol[i]];
            if (!this.FixedRow[i] && this.Attacks[i][this.QueenCol[i]] > 0) { R.push(i); }
        }
        // If no such rows, then nothing more to do
        if (R.length == 0) {
            HaltFlag = true;
            if (totalAttacks == 0) {
                TellUser('Local search succeeded using ' + this.step + ' steps.')
                return;
            }
            TellUser('Error, fixed queens are attacking each other!');
            return;
        }
        // Randomly pick a row with attacks
        let row: number = R[randomInteger(0, R.length - 1)];
        let col: number = this.QueenCol[row];
        let currentAttack: number = this.Attacks[row][col];

        // Find the minimum number of attacks in columns other than current
        let minAttack = this.Dim * this.Dim;
        for (let j = 0; j < this.Dim; j++) {
            if (j != col && this.Attacks[row][j] < minAttack) {
                minAttack = this.Attacks[row][j];
            }
        }
        // Make a list of the minimum attack columns
        let C: number[] = [];
        for (let j = 0; j < this.Dim; j++) {
            if (j != col && this.Attacks[row][j] == minAttack) {
                C.push(j);
            }
        }
        debug && console.log('flip = ', this.step, ' totalAttacks = ', totalAttacks, ' currentAttack = ', currentAttack, ' minAttack = ', minAttack, ' delta = ', minAttack - currentAttack);
        // Move the queen
        let newCol: number = C[randomInteger(0, C.length - 1)];
        // Delete queen from current position and update Attacks
        this.updateAttacks(row, col, Square.Blank);
        // Add queen to new position and update Attacks
        this.updateAttacks(row, newCol, Square.Queen);
        debug && console.log('    Move (', row, ',', col, ') to (', row, ',', newCol, ')');
        this.step++;
        updateStep(this.step);
        // get ready for next step
        if (HaltFlag) {
            TellUser('Local search halted at step ' + this.step +'.');
            return;
        }
        Schedule(LocalSearchSolverStepper);
    }
}


// Global Variables
var nqueens: nQueens;
var delay: number = 500; // milliseconds between solver steps
var colorFlag: boolean = false;
var HaltFlag: boolean = true;
var RestartFlag: boolean = false;
var restartNumber: number = 0;
var cutoff: number = 200; // Only used in usenode mode
var BacktrackGenerator; // Generator object for backtracking search

// Interface Functions

function Schedule(fn) {
    if (usenode) {
        setTimeout(fn, delay);
    }
    else {
        window.setTimeout(fn, delay);
    }
}

function BacktrackingSolverStepper() {
    bug('About to call BacktrackGenerator.next');
    let status = BacktrackGenerator.next();
    debug && console.log('  next() returns ', status);
    if (! HaltFlag) {
        if (RestartFlag){
            restartNumber += 1;
            updateRestart(restartNumber);
            Schedule(initRandRestarts);
            RestartFlag = false;
        }
        else {
            Schedule(BacktrackingSolverStepper);
        }
    }
}

function initRandRestarts(){
    nqueens.StartBacktrackingSolver(true,true);
}

function LocalSearchSolverStepper() {
    nqueens.LocalSearchSolver();
}

function TellUser(msg: string) {
    if (usenode) {
        console.log(msg);
    }
    else {
        let b = document.getElementById('solverResult');
        b.innerHTML = b.innerHTML + '<br />\n' + msg;
        b.scrollTop = b.scrollHeight;
    }
}

function attackColor(a: number): string {
    // convert a number 0 to max number of attacks to a color string
    if (a <= 0) { return 'GREEN'; }
    //   else if (a <= 1) { return '#00D3DB'; }
    else if (a <= 1) { return 'BLUE'; }
    else if (a <= 2) { return 'PURPLE'; }
    else { return 'RED'; }
}

function updateStep(n: number){
    let counter: HTMLElement = document.getElementById("step");
    counter.innerHTML = n.toString();
}

function updateRestart(n: number){
    let counter: HTMLElement = document.getElementById("restart");
    counter.innerHTML = n.toString();
}

function updateChessboard(r: number, c: number, sq: Square, isFixed: boolean = false, useColors: boolean = false) {
    if (usenode) {
        debug && console.log('Chessboard[', r, '][', c, '] = ', sq);
    }
    else {
        let id: string = 'sq-' + r.toString() + '-' + c.toString();
        // debug && console.log('setting cell=', id, " to ", sq);
        let cell: HTMLElement = document.getElementById(id);
        if (sq == Square.Blank) {
            cell.innerHTML = '';
        }
        else {
            // Insert the character for Queen
            let queenchar = isFixed ? '&#9813;' : '&#9819;';
            if (colorFlag && useColors) {
                cell.innerHTML = '<div style="color:' + attackColor(nqueens.Attacks[r][c]) +
                    '">' + queenchar + '</div>';
            }
            else {
                cell.innerHTML = queenchar;
            }
        }
    }
}

function generateChessboardHTML(Dim: number): string {
    let h: string[] = [];
    for (let r: number = 0; r < Dim; r++) {
        h.push('<tr>\n')
        for (let c: number = 0; c < Dim; c++) {
            h.push('<td id="sq-', r.toString(), '-', c.toString(),
                '" name="chessBoardSquare" class="');
            // calculate square color
            // 0,0 light 0,1 dark 0,2 light ...
            // 1,0 dark 1,1 light 1,2 dark ...
            // r+c even is light, r+c odd is dark
            if ((r + c) % 2 == 0) {
                h.push('light');
            }
            else {
                h.push('dark')
            }
            h.push('"></td>\n');
        }
        h.push('</tr>\n')
    }
    return h.join('');
}

function updateSquareStyleDim(d: number) {
    let h = document.documentElement.clientHeight; // units are px
    let sqSize = Math.floor(Math.max(((h - 10) / d) - 4, 8)); // Allow 2 px for border
    document.getElementById('squareSizeStyle').innerHTML =
        '.chess-board td { width:' + sqSize + 'px; height:' + sqSize + 'px; font-size:' +
        (sqSize - 2) + 'px; text-align: center; line-height: ' + (sqSize - 2) + 'px; }'
    debug && console.log('creating style ', document.getElementById('squareSizeStyle').innerHTML);
}

function updateSquareStyle() {
    updateSquareStyleDim(nqueens.Dim);
}

function resizeBoard(d: number) {
    // Can't set dim before creating board, but creating the board 
    updateSquareStyleDim(d);
    document.getElementById('chess-board-body').innerHTML = generateChessboardHTML(d);
    // Add handler to each square
    let elements = document.getElementsByName('chessBoardSquare');
    // elements is a nodeList, not an array, so can't use for ... of
    for (var i = 0; i < elements.length; i++) {
        elements[i].onclick = function () { squareHandler((<HTMLInputElement>this).id); };
    }
    // debug && console.log('creating board ', document.getElementById('chess-board-body').innerHTML);
    // Can't create nQueens until the HTML has been created for it
    nqueens = new nQueens(d);
    updateStep(0);
}

// Button Handlers

function updateFixedInput(numFixed: number){
    (document.getElementById('inputFixed') as HTMLInputElement).value = String(numFixed); 
}

function getCutoff(): number {
    if (usenode) {
        return cutoff;
    }
    return Number((document.getElementById('inputCutoff') as HTMLInputElement).value);
}

function resizeButtonHandler() {
    // Handler for resize button
    resizeBoard(Number((document.getElementById('inputDim') as HTMLInputElement).value));
}

function squareHandler(id: string) {
    let sq: string, rs: string, cs: string;
    debug && console.log('got click on ', id);
    [sq, rs, cs] = id.split('-');
    debug && console.log('calling placeOrDeleteQueen at ', Number(rs), ' ', Number(cs));
    nqueens.PlaceOrDeleteQueen(Number(rs), Number(cs));
}

function haltButtonHandler() {
    HaltFlag = true;
    debug && console.log('clicked on Halt');
    TellUser('Search halted by user.')
}

function colorButtonHandler(isChecked: boolean) {
    colorFlag = isChecked;
    debug && console.log('Color checkbox set to ', isChecked);
    if (isChecked) {
        TellUser('Attacks color code:');
        TellUser('  0 = Green');
        TellUser('  1 = Blue');
        TellUser('  2 = Purple');
        TellUser('  3 or more = Red');
    }
}

function localSearchButtonHandler() {
    nqueens.StartLocalSearchSolver();
}

function clearButtonHandler() {
    nqueens.ClearBoard();
    updateStep(0); 
}

function backtrackingButtonHandler() {
    nqueens.StartBacktrackingSolver(false);
}

function randomBacktrackingButtonHandler() {
    nqueens.StartBacktrackingSolver(true);
}

function randomRestartsButtonHandler(){
    restartNumber = 0;
    updateRestart(restartNumber);
    nqueens.StartBacktrackingSolver(true,true);
}

function placeButtonHandler(){
    let goalFixed = Number((document.getElementById('inputFixed') as HTMLInputElement).value);
    nqueens.placeFixed(goalFixed);
}

function delayButtonHandler(value: string) {
    delay = Number(value) * 175;
    debug && console.log('delay set to ', delay);
}

if (usenode) {
    delay = 0;
    cutoff = 200;
    nqueens = new nQueens(6);
    nqueens.FixQueen(0, 4);
    nqueens.StartBacktrackingSolver(false);
}

function finishBuildingHandlers() {
    console.log("started window.onload");
    document.getElementById('backtrackingButton').onclick = function () { backtrackingButtonHandler(); };
    document.getElementById('randBacktrackingButton').onclick = function () { randomBacktrackingButtonHandler(); };
    document.getElementById('randomRestartsButton').onclick = function () { randomRestartsButtonHandler(); };
    document.getElementById('haltButton').onclick = function () { haltButtonHandler(); }
    document.getElementById('placeButton').onclick = function () { placeButtonHandler(); }
    document.getElementById('resizeButton').onclick = function () { resizeButtonHandler(); }
    document.getElementById('clearButton').onclick = function () { clearButtonHandler(); }
    document.getElementById('localSearchButton').onclick = function () { localSearchButtonHandler(); }
    document.getElementById('colorAttacks').onchange = function () { colorButtonHandler((<HTMLInputElement>this).checked); }
    let elements = document.getElementsByName('delay');
    // elements is a nodeList, not an array, so can't use for ... of
    for (var i = 0; i < elements.length; i++) {
        elements[i].onchange = function () { delayButtonHandler((<HTMLInputElement>this).value); };
    }
    resizeBoard(8);
    window.addEventListener("resize", updateSquareStyle);
}


window.addEventListener('load', function () { finishBuildingHandlers(); });
// The 'load' event does not fire in StackBlitz, so initialization must be called directly
// by uncommenting the following line:
// finishBuildingHandlers();
